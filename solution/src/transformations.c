#include "image.h"
#include <stdlib.h>
struct image complete(int angle, struct image source_image) {
    int num_rotations = (abs(angle) % 360) / 90;
    if (angle < 0) {
        num_rotations = 4 - num_rotations;
    }
    for (int i = 0; i < num_rotations; i++) {
        struct image rotated_image = rotate(source_image);

        free(source_image.data);

        source_image = rotated_image;
    }

    return source_image;
}
