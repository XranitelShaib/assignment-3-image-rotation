#include "../include/bmp.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const uint32_t HEADER_SIZE = 40;
const uint32_t COLOR_PLANE = 1;
const uint32_t BITS_ON_PIXEL = 24;
const uint32_t NO_COMP = 0;

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static void write_header(FILE *out, struct image const *img) {
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + (img->width * img->height * sizeof(struct pixel));
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);

    header.biSize = HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = COLOR_PLANE;
    header.biBitCount = BITS_ON_PIXEL;
    header.biCompression = NO_COMP;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    fwrite(&header, sizeof(struct bmp_header), 1, out);
}

size_t calculate_padding(uint64_t width) {
    size_t pixel_size = sizeof(struct pixel);
    return (4 - (width * pixel_size % 4)) % 4;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (out == NULL || img == NULL) {
        return WRITE_ERROR;
    }

    write_header(out, img);

    for (uint64_t y = 0; y < img->height; y++) {
        for (uint64_t x = 0; x < img->width; x++) {
            struct pixel p = img->data[y * img->width + x];

            if (fwrite(&p, sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }

        size_t padding = calculate_padding(img->width);
        if (padding > 0) {
            uint8_t pad_byte = 0;
            for (size_t i = 0; i < padding; i++) {
                if (fwrite(&pad_byte, 1, 1, out) != 1) {
                    return WRITE_ERROR;
                }
            }
        }
    }
    return WRITE_OK;
}

static int read_header(FILE *in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return 0;
    }

    if (header->bfType != 0x4D42) {
        return 0;
    }

    return 1;
}

static int read_pixels(FILE *in, struct image *img) {
    uint64_t width = img->width;
    uint64_t height = img->height;
    size_t row_size = width * sizeof(struct pixel);
    size_t padding = (4 - (row_size % 4)) % 4;

    img->data = (struct pixel *) malloc(width * height * sizeof(struct pixel));

    if (img->data == NULL) {
        return 0;
    }

    for (uint64_t y = 0; y < height; y++) {
        if (fread(&img->data[y * width], sizeof(struct pixel), width, in) != width) {
            return 0;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            return 0;
        }
    }

    return 1;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;

    if (!read_header(in, &header)) {
        fprintf(stderr, "Ошибка: неправильная сигнатура BMP файла.\n");
        return READ_INVALID_SIGNATURE;
    }

    if (header.bfType != 0x4D42 || header.biBitCount != 24 || header.biCompression != 0) {
        fprintf(stderr, "Ошибка: неподдерживаемый формат BMP файла.\n");
        return READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    if (!read_pixels(in, img)) {
        fprintf(stderr, "Ошибка: неправильный заголовок BMP файла или ошибка чтения пикселей.\n");
        return READ_INVALID_HEADER;
    }
    


    return READ_OK;
}
