#include "../include/image.h"
#include <stdlib.h>

/* source may be an invalid image with a null pointer */
struct image rotate(struct image const source) {
    struct image result;
    result.width = source.height;
    result.height = source.width;
    result.data = (struct pixel*)malloc(result.width * result.height * sizeof(struct pixel));

    if (result.data == NULL) {
        result.width = 0;
        result.height = 0;
        return result;
    }

    for (uint64_t x = 0; x < source.width; x++) {
        for (uint64_t y = 0; y < source.height; y++) {
            struct pixel src_pixel = source.data[y * source.width + x];

            uint64_t new_x = y;
            uint64_t new_y = result.height - x - 1;

            result.data[new_y * result.width + new_x] = src_pixel;
        }
    }


    return result;
}
