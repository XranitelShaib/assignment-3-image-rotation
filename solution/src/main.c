#include "bmp.h"
#include "image.h"
#include "transformations.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Использование: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        exit(1);
    }

    char *source_image_filename = argv[1];
    char *transformed_image_filename = argv[2];
    int angle = atoi(argv[3]);

    if (angle % 90 != 0) {
        fprintf(stderr, "Угол поворота должен быть кратным 90 градусам.\n");
        exit(1);
    }

    struct image img;
    FILE *source_file = fopen(source_image_filename, "rb");

    if (source_file == NULL) {
        perror("Ошибка открытия исходного файла");
        exit(1);
    }

    enum read_status read_result = from_bmp(source_file, &img);
    fclose(source_file);

    if (read_result != READ_OK) {
        free(img.data);
        fprintf(stderr, "Ошибка при чтении изображения.\n");
        exit(1);
    }

    if (angle != 0) {
        struct image rotated_img = complete(angle, img);
        img = rotated_img;
    }

    FILE *transformed_file = fopen(transformed_image_filename, "wb");

    if (transformed_file == NULL) {
        perror("Ошибка открытия файла для записи");
        free(img.data);
        exit(1);
    }

    enum write_status write_result = to_bmp(transformed_file, &img);
    fclose(transformed_file);
    free(img.data);

    if (write_result != WRITE_OK) {
        fprintf(stderr, "Ошибка при записи изображения.\n");
        exit(1);
    }

    printf("Изображение успешно обработано и сохранено в %s\n", transformed_image_filename);

    return 0;
}
