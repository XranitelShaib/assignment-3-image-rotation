#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image rotate(struct image const source);

#endif  // IMAGE_H
